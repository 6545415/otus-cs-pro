﻿namespace ConsoleApp.Entities
{
    class User
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public int Age { get; set; }
        public Course Course { get; set; }
        public UserRole UserRole { get; set; }
    }
}
