﻿using ConsoleApp.Database;
using ConsoleApp.Entities;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;

namespace ConsoleApp
{
    class Program
    {
        static void Main()
        {
            var isRunning = true;
            while (isRunning)
            {
                Console.Clear();
                Console.WriteLine("Press:");
                Console.WriteLine("1 - Seed Database");
                Console.WriteLine("2 - Print Database");
                Console.WriteLine("3 - Add");
                Console.WriteLine("other - Exit");
                var key = Console.ReadKey();
                switch (key.KeyChar)
                {
                    case '1':
                        DbSeed();
                        break;
                    case '2':
                        DbPrint();
                        break;
                    case '3':
                        DbAdd();
                        break;
                    default:
                        isRunning = false;
                        break;
                }
            }
        }

        private static void DbAdd()
        {
            Console.Clear();
            Console.WriteLine("1 - Add a User");
            Console.WriteLine("2 - Add a UserRole");
            Console.WriteLine("3 - Add a Course");
            var key = Console.ReadKey();
            switch (key.KeyChar)
            {
                case '1':
                    AddUser();
                    break;
                case '2':
                    AddUserRole();
                    break;
                case '3':
                    AddCourse();
                    break;
            }
        }

        private static void AddCourse()
        {
            using var db = new ApplicationContext();
            var course = new Course();

            Console.Clear();

            Console.WriteLine("Enter Course Name: ");
            course.Name = Console.ReadLine();

            db.Courses.Add(course);
            db.SaveChanges();
        }

        private static void AddUserRole()
        {
            using var db = new ApplicationContext();
            var role = new UserRole();

            Console.Clear();

            Console.WriteLine("Enter User Role Name: ");
            role.Name = Console.ReadLine();

            db.UserRoles.Add(role);
            db.SaveChanges();
        }

        private static void AddUser()
        {
            using var db = new ApplicationContext();
            var user = new User();

            Console.Clear();

            Console.WriteLine("Enter User Name: ");
            user.Name = Console.ReadLine();
            Console.WriteLine("Enter User Age: ");
            user.Age = int.Parse(Console.ReadLine() ?? "0");
            Console.WriteLine($"Enter User Course Id ({db.Courses.Max(c => c.Id)} MAX): ");
            user.Course = db.Courses.First(c => c.Id == int.Parse(Console.ReadLine() ?? "0"));
            Console.WriteLine($"Enter User Role Id ({db.UserRoles.Max(c => c.Id)} MAX): ");
            user.UserRole = db.UserRoles.First(c => c.Id == int.Parse(Console.ReadLine() ?? "0"));

            db.Users.Add(user);
            db.SaveChanges();
        }

        private static void DbPrint()
        {
            Console.Clear();
            using var db = new ApplicationContext();
            var users = db.Users
                .Include(u => u.Course)
                .Include(u => u.UserRole)
                .ToList();

            Console.WriteLine(new string('=', 40));
            Console.WriteLine("Users:");
            Console.WriteLine(new string('-', 40));
            foreach (var u in users)
            {
                Console.WriteLine($"{u.Id}.{u.Name} - {u.Age} at \"{u.Course.Name}\" as a {u.UserRole.Name}");
            }

            Console.WriteLine(new string('=', 40));

            var courses = db.Courses.ToList();
            Console.WriteLine("Courses:");
            Console.WriteLine(new string('-', 40));
            foreach (var c in courses)
            {
                Console.WriteLine($"{c.Name}");
            }

            Console.WriteLine(new string('=', 40));

            var roles = db.UserRoles.ToList();
            Console.WriteLine("Roles:");
            Console.WriteLine(new string('-', 40));
            foreach (var c in roles)
            {
                Console.WriteLine($"{c.Name}");
            }

            Console.WriteLine(new string('=', 40));

            Console.ReadKey();
        }

        private static void DbSeed()
        {
            Console.Clear();
            using var db = new ApplicationContext();
            var courses = new List<Course>
            {
                new Course {Name = "C++ Pro"},
                new Course {Name = "C# Pro"},
                new Course {Name = "C++ Base"},
                new Course {Name = "C# Base"},
                new Course {Name = "PHP"},
                new Course {Name = "JS"},
                new Course {Name = "Python"},
            };

            var roles = new List<UserRole>
            {
                new UserRole {Name = "Tutor"},
                new UserRole {Name = "Student"},
                new UserRole {Name = "Maintainer"},
                new UserRole {Name = "Mentor"},
                new UserRole {Name = "Leader"},
            };

            var users = new List<User>
            {
                new User {Name = "Tom", Age = 33, Course = courses[0], UserRole = roles[0]},
                new User {Name = "Alice", Age = 26, Course = courses[3], UserRole = roles[1]},
                new User {Name = "Peter", Age = 33, Course = courses[1], UserRole = roles[0]},
                new User {Name = "John", Age = 34, Course = courses[4], UserRole = roles[0]},
                new User {Name = "Ivan", Age = 21, Course = courses[2], UserRole = roles[1]},
                new User {Name = "Jack", Age = 17, Course = courses[1], UserRole = roles[1]},
                new User {Name = "Katie", Age = 20, Course = courses[1], UserRole = roles[1]},
            };

            db.Courses.AddRange(courses);
            db.UserRoles.AddRange(roles);
            db.Users.AddRange(users);

            db.SaveChanges();
        }
    }
}