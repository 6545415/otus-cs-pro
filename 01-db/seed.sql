INSERT INTO public."Courses"("Name") VALUES ('C++ Pro');
INSERT INTO public."Courses"("Name") VALUES ('C# Pro');
INSERT INTO public."Courses"("Name") VALUES ('C++ Base');
INSERT INTO public."Courses"("Name") VALUES ('C# Base');
INSERT INTO public."Courses"("Name") VALUES ('PHP');
INSERT INTO public."Courses"("Name") VALUES ('JS');
INSERT INTO public."Courses"("Name") VALUES ('Python');

INSERT INTO public."UserRoles"("Name")	VALUES ('Tutor');
INSERT INTO public."UserRoles"("Name")	VALUES ('Student');
INSERT INTO public."UserRoles"("Name")	VALUES ('Maintainer');
INSERT INTO public."UserRoles"("Name")	VALUES ('Mentor');
INSERT INTO public."UserRoles"("Name")	VALUES ('Leader');

INSERT INTO public."Users"("Name", "Age", "CourseId", "UserRoleId")	VALUES ('Tom', 33, 1, 1);
INSERT INTO public."Users"("Name", "Age", "CourseId", "UserRoleId")	VALUES ('Alice', 26, 3, 2);
INSERT INTO public."Users"("Name", "Age", "CourseId", "UserRoleId")	VALUES ('Peter', 33, 4, 2);
INSERT INTO public."Users"("Name", "Age", "CourseId", "UserRoleId")	VALUES ('John', 34, 2, 3);
INSERT INTO public."Users"("Name", "Age", "CourseId", "UserRoleId")	VALUES ('Ivan', 21, 1, 5);
INSERT INTO public."Users"("Name", "Age", "CourseId", "UserRoleId")	VALUES ('Jack', 17, 1, 4);
INSERT INTO public."Users"("Name", "Age", "CourseId", "UserRoleId")	VALUES ('Katie', 20, 2, 1);
