using System.Linq;
using Microsoft.AspNetCore.Mvc;
using WebApi.Database;
using WebApi.Models;

namespace WebApi.Controllers
{
    [Route("customers")]
    public class CustomerController : Controller
    {
        [HttpGet("{id:long}")]
        public Customer GetCustomerAsync([FromRoute] long id)
        {
            using var db = new ApplicationContext();

            return db.Customers.First(c => c.Id == id);
        }

        [HttpPost("")]
        public long CreateCustomerAsync([FromBody] Customer customer)
        {
            using var db = new ApplicationContext();

            var newCustomer = db.Customers.Add(new Customer
                {Firstname = customer.Firstname, Lastname = customer.Lastname});

            db.SaveChanges();

            return newCustomer.Entity.Id;
        }
    }
}