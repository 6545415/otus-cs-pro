﻿using Microsoft.EntityFrameworkCore;
using WebApi.Models;

namespace WebApi.Database
{
    public sealed class ApplicationContext : DbContext
    {
        public DbSet<Customer> Customers { get; set; }

        public ApplicationContext()
        {
            Database.EnsureCreated();
        }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseNpgsql("Host=localhost;Port=5432;Database=otus;Username=postgres;Password=1");
        }
    }
}