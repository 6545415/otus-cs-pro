﻿using System;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace WebClient
{
    static class Program
    {
        static async Task Main()
        {
            var isRunning = true;
            while (isRunning)
            {
                Console.Clear();
                Console.WriteLine("Press:");
                Console.WriteLine("1 - Get Customer by id");
                Console.WriteLine("2 - Create Random Customer");
                Console.WriteLine("other - Exit");
                var key = Console.ReadKey();
                string id;
                switch (key.KeyChar)
                {
                    case '1':
                        Console.Clear();
                        id = Console.ReadLine() ?? "0";
                        try
                        {
                            PrintCustomer(await GetCustomerAsync(int.Parse(id)), id);
                        }
                        catch (Exception e)
                        {
                            Console.WriteLine(e.Message);
                        }

                        break;
                    case '2':
                        Console.Clear();
                        id = await PostCustomerAsync(CreateRandomCustomer());
                        Console.WriteLine($"Random Customer successfully added with id: {id}");
                        Console.ReadKey();
                        break;
                    default:
                        isRunning = false;
                        break;
                }
            }
        }

        private static async Task<Customer> GetCustomerAsync(int id)
        {
            var response = await new HttpClient().GetAsync($"https://localhost:5001/customers/{id}");

            return JsonConvert.DeserializeObject<Customer>(await response.Content.ReadAsStringAsync());
        }

        private static async Task<string> PostCustomerAsync(CustomerCreateRequest customer)
        {
            var response = await new HttpClient().PostAsync("https://localhost:5001/customers",
                new StringContent(JsonConvert.SerializeObject(new Customer
                        {Firstname = customer.Firstname, Lastname = customer.Lastname}),
                    Encoding.UTF8,
                    "application/json"));

            return JsonConvert.DeserializeObject<string>(await response.Content.ReadAsStringAsync());
        }

        private static void PrintCustomer(Customer customer, string id)
        {
            if (customer == null)
            {
                Console.WriteLine($"There is no customer with id = {id}");
                Console.ReadKey();
                return;
            }

            Console.WriteLine($"#{customer.Id} - {customer.Firstname} {customer.Lastname}");
            Console.ReadKey();
        }

        private static CustomerCreateRequest CreateRandomCustomer()
        {
            var random = new Random();
            return new CustomerCreateRequest
            {
                Firstname = $"RandomFirstName{random.Next(int.MaxValue)}",
                Lastname = $"RandomLastName{random.Next(int.MaxValue)}",
            };
        }
    }
}