﻿namespace _03_GuessNumberGame.Config
{
    internal class ConfigModel
    {
        public readonly int AttemptsAmount;
        public readonly int MinValue;
        public readonly int MaxValue;

        public ConfigModel(int attemptsAmount, int minValue, int maxValue)
        {
            AttemptsAmount = attemptsAmount;
            MinValue = minValue;
            MaxValue = maxValue;
        }
    }
}