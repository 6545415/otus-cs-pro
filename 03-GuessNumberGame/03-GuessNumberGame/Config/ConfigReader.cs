﻿using System.IO;
using Microsoft.Extensions.Configuration;

namespace _03_GuessNumberGame.Config
{
    internal class ConfigReader : IConfigReader
    {
        public ConfigModel GetConfig()
        {
            var builder = new ConfigurationBuilder()
                .SetBasePath(Directory.GetCurrentDirectory())
                .AddJsonFile("config.json", optional: false);

            IConfiguration config = builder.Build();

            return config.GetSection("AppConfig").Get<ConfigModel>();
        }
    }
}