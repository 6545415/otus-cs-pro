﻿namespace _03_GuessNumberGame.Config
{
    internal interface IConfigReader
    {
        ConfigModel GetConfig();
    }
}