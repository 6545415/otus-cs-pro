﻿namespace _03_GuessNumberGame.Core
{
    interface IApplication
    {
        void Run();
    }
}