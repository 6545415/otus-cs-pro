﻿namespace _03_GuessNumberGame.Core
{
    interface IGame
    {
        void Run();
    }
}