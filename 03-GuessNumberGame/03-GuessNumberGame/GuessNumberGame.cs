﻿using System;
using _03_GuessNumberGame.Core;

namespace _03_GuessNumberGame
{
    internal class GuessNumberGame : IGame
    {
        private readonly Random _random = new();

        private readonly int _n;
        private bool _isGameOver;

        public GuessNumberGame()
        {
            _n = _random.Next(100);
        }

        public void Run()
        {
            while (!_isGameOver)
            {
#if DEBUG
                var msg = $"Enter your number ({_n}): ";
#else
                const string msg = "Enter your number: ";
#endif
                Console.WriteLine(msg);
                if (!int.TryParse(Console.ReadLine() ?? string.Empty, out var enteredN))
                {
                    Console.WriteLine("ERROR: Entered value is not a number!");
                    continue;
                }

                _isGameOver = enteredN == _n;
                Console.WriteLine(GetMsg(enteredN));
            }
        }

        private string GetMsg(int enteredN) =>
            enteredN > _n
                ? "My number is lower"
                : enteredN < _n
                    ? "My number is higher"
                    : $"You win! The number is {_n}";
    }
}