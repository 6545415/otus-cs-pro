﻿using _03_GuessNumberGame.Core;

namespace _03_GuessNumberGame.Platform.Windows
{
    class WindowsApplication : IApplication
    {
        private readonly IGame _game;

        public WindowsApplication(IGame game)
        {
            _game = game;
        }

        public void Run()
        {
            _game.Run();
        }
    }
}