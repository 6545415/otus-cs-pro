﻿using _03_GuessNumberGame.Platform.Windows;

namespace _03_GuessNumberGame
{
    static class Program
    {
        static void Main(string[] args)
        {
            var app = new WindowsApplication(new GuessNumberGame());
            app.Run();
        }
    }
}